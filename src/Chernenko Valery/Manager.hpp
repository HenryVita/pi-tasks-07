#pragma once

#include "Employee.hpp"

class Manager: public Employee, protected Project
{
	
protected:

	int parts;
	MyProject* project;

	Manager(int id, string fio) : Employee(id, fio)
	{

	}

public:
	
	Manager(int id, string fio, int parts, MyProject* project) : Employee(id, fio)
	{
		this->parts = parts;
		this->project = project;
	}

	Manager(Manager& manager) : Employee(manager)
	{
		parts = manager.parts;
		project = manager.project;
	}

	~Manager()
	{

	}

	void setPayment()
	{
		payment = (int)SetProjectPayment(parts, project);
	}

};

class ProjectManager: public Manager, protected Heading
{

protected:

	int rate;

	ProjectManager(int id, string fio, int rate): Manager(id, fio)
	{
		this->rate = rate;
	}

public:

	ProjectManager(int id, string fio, int rate ,int parts, MyProject* project) : Manager(id, fio, parts, project)
	{
		this->rate = rate;
	}
	
	ProjectManager(ProjectManager& manager) : Manager(manager)
	{

	}

	~ProjectManager()
	{

	}

	void setPayment()
	{
		payment = (int)SetProjectPayment(parts, project) + SetHeadingPayment(rate, project->getManager(), workTime);
	}
};

class SeniorManager: public ProjectManager
{

private:

	const int parts = 0;
	const MyProject* project = nullptr;

protected:

	struct SeniorProject
	{
		MyProject* project;
		int parts;
	};

	int countProj;
	SeniorProject* projects;

public:

	SeniorManager(int id, string fio, int countProj, MyProject** project, int* parts, int rate): ProjectManager(id, fio, rate)
	{
		this->countProj = countProj;
		projects = new SeniorProject[countProj];
		for (int i = 0; i < countProj; ++i)
		{
			projects[i].project = project[i];
			projects[i].parts = parts[i];
		}
	}

	SeniorManager(SeniorManager& sManager) : ProjectManager(sManager.id, sManager.fio, sManager.rate)
	{
		countProj = sManager.countProj;
		projects = new SeniorProject[countProj];
		for (int i = 0; i < countProj; ++i)
		{
			projects[i].project = sManager.projects[i].project;
			projects[i].parts = sManager.projects[i].parts;
		}
	}


	~SeniorManager()
	{
		delete[] projects;
	}

	void setPayment()
	{
		payment = SetHeadingPayment(rate, countProj, workTime);
		for (int i = 0; i < countProj; ++i)
		{
			payment += SetProjectPayment(projects[i].parts, projects[i].project);
		}
	}
};