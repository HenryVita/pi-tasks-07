#pragma once

#include "Employee.hpp"

class Personal: public Employee, protected WorkTime
{
protected:

	int rate; //������ �� ���;

public:

	Personal(int id, string fio, int rate): Employee(id, fio)
	{
		this->rate = rate;
	}

	Personal(Personal& personal): Employee(personal)
	{
		rate = personal.rate;
	}

	~Personal()
	{

	}

	void setPayment()
	{
		payment = setWorkTimePayment(rate, workTime);
	}

};

class Cleaner: public Personal
{

public:

	Cleaner(int id, string fio, int rate): Personal(id, fio, rate)
	{

	}

	Cleaner(Cleaner& cleaner): Personal(cleaner)
	{

	}

	~Cleaner()
	{

	}

};

class Driver: public Personal
{

public:

	Driver(int id, string fio, int rate): Personal(id, fio, rate)
	{

	}

	Driver(Driver& driver): Personal(driver)
	{

	}

	~Driver()
	{

	}

};