#include"Employee.hpp"
#include"Engineer.hpp"
#include"Manager.hpp"
#include"Personal.hpp"

#include <string>
#include<iostream>
#include <fstream>
using namespace std;

const int Step = 10;

int countProj;
int countEmployee;

MyProject** projects;
Employee** employee;

MyProject* FindProject(string name)
{
	for (int i = 0; i < countProj; ++i)
		if (projects[i]->getName() == name)
			return projects[i];
	return nullptr;
}

int FindProjectID(string name)
{
	for (int i = 0; i < countProj; ++i)
		if (projects[i]->getName() == name)
			return i;
	return -1;
}

int readFile()
{
	ifstream fin;
	fin.open("StaffDemo.txt");
	if (!fin)
	{
		cout << "File not Found!";
		return 1;
	}

	string buf;
	fin >> buf;
	if (buf != "Projects:")
	{
		cout << "File not corrected!" << endl;
		fin.close();
		return 2;
	}

	countProj = 0;
	fin >> buf;
	while (buf != "Personal:")
	{
		++countProj;
		fin >> buf;
	}
	countProj /= 2;
	if (countProj == 0)
	{
		fin.close();
		return 3;
	}

	fin.seekg(ios_base::beg);
	fin >> buf;
	projects = new MyProject*[countProj];
	for (int i = 0; i < countProj; ++i)
	{
		string name;
		int budget;
		fin >> name;
		fin >> budget;
		projects[i] = new MyProject(name, budget);
	}
	fin >> buf;
	employee = new Employee*[Step];
	for (countEmployee = 0; fin;)
	{
		int id;
		fin >> id;
		string fam, nam;
		fin >> fam;
		fin >> nam;
		fam += " " + nam;
		string spec;
		fin >> spec;

		if (spec == "Employee")
		{
			employee[countEmployee] = new Employee(id, fam);
		}
		else if (spec == "Personal" || spec == "Cleaner" || spec == "Driver")
		{
			int rate;
			fin >> rate;
			if (spec == "Personal")
				employee[countEmployee] = new Personal(id, fam, rate);
			else if (spec == "Cleaner")
				employee[countEmployee] = new Cleaner(id, fam, rate);
			else
				employee[countEmployee] = new Driver(id, fam, rate);
		}
		else if (spec == "Engineer" || spec == "Tester" || spec == "Programmer" || spec == "TeamLeader")
		{
			int rate;
			fin >> rate;
			string projectName;
			fin >> projectName;
			int parts;
			fin >> parts;
			MyProject* project = FindProject(projectName);
			if (!project)
			{
				fin.close();
				cout << "������ �� ������!";
				return 4;
			}
			if (spec == "Engineer")
				employee[countEmployee] = new Engineer(id, fam, rate, parts, project);
			else if(spec == "Tester")
				employee[countEmployee] = new Tester(id, fam, rate, parts, project);
			else if (spec == "Programmer")
			{
				employee[countEmployee] = new Programmer(id, fam, rate, parts, project);
				project->addProgrammer();
			}
			else
			{
				int ratePerPerson;
				fin >> ratePerPerson;
				employee[countEmployee] = new TeamLeader(id, fam, rate, parts, project, ratePerPerson);
			}
			project->addParts(parts);
		}
		else if (spec == "Manager" || spec == "ProjectManager")
		{
			string projectName;
			fin >> projectName;
			MyProject* project = FindProject(projectName);
			if (!project)
			{
				fin.close();
				cout << "������ �� ������!";
				return 4;
			}
			int parts;
			fin >> parts;
			if (spec == "Manager")
			{
				employee[countEmployee] = new Manager(id, fam, parts, project);
				project->addManager();
			}
			else
			{
				int rate;
				fin >> rate;
				employee[countEmployee] = new ProjectManager(id, fam, rate, parts, project);
			}
			project->addParts(parts);
		}
		else if (spec == "SeniorManager")
		{
			int* parts = new int[countProj];
			for (int j = 0; j < countProj; j++)
			{
				string projectName;
				fin >> projectName;
				int part;
				fin >> part;
				int projectId = FindProjectID(projectName);
				if (projectId == -1)
				{
					fin.close();
					cout << "Project not Found!!!" <<endl;
					return 5;
				}
				parts[projectId] = part;
				projects[projectId]->addParts(part);
			}
			int rate;
			fin >> rate;
			employee[countEmployee] = new SeniorManager(id, fam, countProj, projects, parts, rate);
			delete parts;
		}

		countEmployee++;
		if (countEmployee % Step == 0)
		{
			Employee**tmp = new Employee*[countEmployee + Step];
			for (int j = 0; j < countEmployee; j++)
				tmp[j] = employee[j];
			delete[] employee;
			employee = new Employee*[countEmployee + Step];
			for (int j = 0; j < countEmployee; j++)
				employee[j] = tmp[j];
			delete[] tmp;
		}

	}
	countEmployee--;
	fin.close();
	return 0;
}

void AllWorking(int hours)
{
	for (int i = 0; i < countEmployee; i++)
		employee[i]->Working(hours);
}

void SetAllPayment()
{
	for (int i = 0; i < countEmployee; i++)
	{
		employee[i]->setPayment();
		employee[i]->SeePayment();
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << readFile()<<endl;
	cout << "���������� �������� = " << countProj << endl;
	cout << "���������� ���������� = " << countEmployee << endl;
	AllWorking(20);
	SetAllPayment();


	for (int i = 0; i < countProj; i++)
		delete projects[i];
	delete[] projects;
	for (int i = 0; i < countEmployee; i++)
		delete employee[i];
	delete[] employee;
}