#pragma once

#include <string>
#include <iostream>
using namespace std;

extern const int MinimalSalary = 8999;

class MyProject
{
private:

	string name;		//�������� �������;
	int budget;			//������;
	int parts;			//���������� ������;
	int countProg;		//���������� �������������;
	int countManager;	//���������� ����������;

public:

	MyProject(string name, int budget)
	{
		this->name = name;
		this->budget = budget;
		parts = 0;
		countProg = countManager = 0;
	}

	MyProject(MyProject& project)
	{
		name = project.name;
		budget = project.budget;
		parts = project.parts;
		countProg = project.countProg;
		countManager = project.countManager;
	}

	~MyProject()
	{

	}

	//�������;
	string getName() const { return name; }
	int getBudget() const { return budget; }
	int getParts() const { return parts; }
	int getProgrammer() const { return countProg; }
	int getManager() const { return countManager; }

	void addParts(int parts) 
	{ 
		this->parts += parts; 
	}
	void addProgrammer() { countProg++; }
	void addManager() { countManager++;  }

};

class Employee
{

protected:

	int id; //��������������� �����;
	string fio; //�������, ��� � ��������;
	int workTime; //������� ����� (��� ���������� ��������);
	int payment;  //��������;

public:

	Employee(int id, string fio)
	{
		this->id = id;
		this->fio = fio;
		payment = MinimalSalary;
		workTime = 0;
	}

	Employee(Employee& emp)
	{
		id = emp.id;
		fio = emp.fio;
		payment = emp.payment;
		workTime = emp.workTime;
	}

	~Employee()
	{

	}

	void SeePayment()
	{
		cout << id << " " << fio << " " << payment << endl;
	}

	//�������;
	int getPayment() const { return payment; }

	//��������!;
	void Working(int hours)
	{
		workTime+=hours;
	}

	//��������� ��������;
	virtual void setPayment()
	{
		payment = MinimalSalary;
	}

};

class WorkTime
{

protected:

	int setWorkTimePayment(int rate, int workTime)
	{
		return rate*workTime;
	}

};

class Project
{

protected:

	int SetProjectPayment(int parts, MyProject* project)
	{
		return parts * project->getBudget() / project->getParts();
	}
};

class Heading
{

protected:

	int SetHeadingPayment(int rate, int countPeople, int workTime)
	{
		return rate*countPeople*workTime;
	}
};

