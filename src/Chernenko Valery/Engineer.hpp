#pragma once

#include "Employee.hpp"

class Engineer: public Employee, protected WorkTime, protected Project
{

protected:

	int rate;
	int parts;				//����� � ������;
	MyProject* project;		//������ � ������� ������������;

public:

	Engineer(int id, string fio, int rate ,int parts, MyProject* project) : Employee(id, fio)
	{
		this->rate = rate;
		this->parts = parts;
		this->project = project;
	}

	Engineer(Engineer& engineer) : Employee(engineer)
	{
		rate = engineer.rate;
		parts = engineer.parts;
		project = engineer.project;
	}

	~Engineer()
	{

	}

	void setPayment()
	{
		payment = SetProjectPayment(parts , project) + setWorkTimePayment(rate,workTime);
	}

};

class Tester: public Engineer
{

public: 

	Tester(int id, string fio,int rate, int parts, MyProject* project) : Engineer(id, fio, rate ,parts, project)
	{

	}

	Tester(Tester& tester) : Engineer(tester)
	{

	}

	~Tester()
	{

	}

};

class Programmer: public Engineer
{

public:

	Programmer(int id, string fio, int rate, int parts, MyProject* project) : Engineer(id, fio, rate, parts, project)
	{
		
	}

	Programmer(Programmer& programmer): Engineer(programmer)
	{

	}

	~Programmer()
	{

	}
};

class TeamLeader: public Programmer, protected Heading
{
private:

	int ratePerPerson; //C����� �� ���������� � ���;
	
public:

	TeamLeader(int id, string fio, int rate, int parts, MyProject* project, int ratePerPerson) : Programmer(id, fio, rate, parts, project)
	{
		this->ratePerPerson = ratePerPerson;
	}

	TeamLeader(TeamLeader& leader) : Programmer(leader)
	{
		ratePerPerson = leader.ratePerPerson;
	}

	~TeamLeader()
	{

	}

	void setPayment()
	{ 
		payment = setWorkTimePayment(rate, workTime) + SetProjectPayment(parts, project) + SetHeadingPayment(rate, project->getProgrammer(), workTime);
	}
}; 